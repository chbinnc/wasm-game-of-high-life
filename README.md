<div align="center">

  <h1><code>wasm-game-of-life</code></h1>

  <strong>Yet another extended version of the <a href="https://rustwasm.github.io/book">WASM Game of Life example project</a>.</strong>

</div>

## 📚 About

The default example is discovered from https://f-droid.org/packages/org.jtb.droidlife/

Compared to Life (S23B3), High Life is S23B36,  means a cell will survive if 2 or 3 cells in neighbor, will be born if 3 or 6 cells in neighbor.

An compiled instance is inside ./pkg-mod, you can view it in your favorite browser by `cd pkg-mod && python -m http.server`

## 🎮 Features

* Start/Stop button
* Random universe generation
* Clear button
* Delay slider
* Change universe size
* Click to toggle cells
* Hold mouse button and drag to toggle multiple cells
* Works without Node.js / npm

## 🗺 Roadmap

Please check upstream for roadmap.

## 🚲 Usage

### 🛠️ Needed tools

* [Standard rust toolchain](https://www.rust-lang.org/tools/install)
* [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/)


### 📦 Build with `wasm-pack build`

```
wasm-pack build --target web
```

### 🔍 Use the debug flag for tracing errors

```
wasm-pack build --debug --target web
```

### 🎬 Run on test server with `cargo run --example deploy`

```
cargo run --example deploy
```

### ⚙ Test in Headless Browsers with `wasm-pack test`

Note: There are no tests yet
```
wasm-pack test --headless --firefox
```
