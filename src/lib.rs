mod utils;

use wasm_bindgen::prelude::*;
use std::fmt;
use js_sys::Math;
use fixedbitset::FixedBitSet;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

// lifted from the `console_log` example
#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(a: &str);
}

macro_rules! console_log {
    ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}

#[wasm_bindgen]
pub fn wasm_memory() -> JsValue {
    wasm_bindgen::memory()
}

#[wasm_bindgen]
/// Universe for Conways Game of Life
pub struct Universe {
    width: u32,
    height: u32,
    cells: FixedBitSet,
}

/// Private Universe functions
impl Universe {
    /// returns the index of a cell
    fn get_index(&self, row: u32, column: u32) -> usize {
        (row * self.width + column) as usize
    }

    /// Returns the count of living neighbors of a cell
    fn live_neighbor_count(&self, row: u32, column: u32) -> u8 {
        let mut count = 0;
        for delta_row in [self.height - 1, 0, 1].iter().cloned() {
            for delta_column in [self.width - 1, 0, 1].iter().cloned() {
                if delta_row == 0 && delta_column == 0 {
                    continue;
                }

                let neighbor_row = (row + delta_row) % self.height;
                let neighbor_column = (column + delta_column) % self.width;
                let idx = self.get_index(neighbor_row, neighbor_column);
                count += self.cells[idx] as u8;
            }
        }
        count
    }
    
    /// Creates a lightweigth spaceship (LWSS)
    fn lwss() -> Self {
        let width: u32 = 5;
        let height: u32 = 4;

        let length = (width * height) as usize;
        let data = vec![0b01001_10000_10001_11110];

        let cells = FixedBitSet::with_capacity_and_blocks(length, data);

        Self {
            width,
            height,
            cells,
        }
    }
}

/// Public methods, exported to JavaScript.
#[wasm_bindgen]
impl Universe {
    /// Creates a new blank universe
    pub fn new(width: u32, height: u32) -> Self {

        let size = (width * height) as usize;
        let cells = FixedBitSet::with_capacity(size);
        
        Self {
            width,
            height,
            cells,
        }
    }

    /// Creates a new random universe with 50% chance for cells to be alive
    pub fn new_random(width: u32, height: u32) -> Self {

        let size = (width * height) as usize;
        let mut universe = Self::new(width, height);

        for i in 0..size {
            universe.cells.set(i, Math::random() >= 0.5);
        }

        universe
    }

    /// Create a new universe with a single spaceship (LWSS)
    pub fn new_with_spaceship(width: u32, height: u32) -> Self {
        
        let mut universe = Self::new(width, height);

        universe.insert(&Universe::lwss(), width / 2, height / 2);

        universe
    }

    /// Getter for width
    pub fn width(&self) -> u32 {
        self.width
    }

    /// Getter for height
    pub fn height(&self) -> u32 {
        self.height
    }

    /// Getter for cells (returns a pointer to cells)
    pub fn cells(&self) -> *const u32 {
        self.cells.as_slice().as_ptr()
    }

    /// Calculate next step for universe
    pub fn tick(&mut self) {
        let mut next = self.cells.clone();

        for row in 0..self.height {
            for col in 0..self.width {
                let idx = self.get_index(row, col);
                let cell = self.cells[idx];
                let live_neighbors = self.live_neighbor_count(row, col);

                next.set(idx, match (cell, live_neighbors) {
                    // Rule 1: Underpopulation
                    (true, x) if x < 2 => false,
                    // Rule 2: Stay Alive
                    (true, 2) | (true, 3) => true,
                    // Rule 3: Overpopulation
                    (true, x) if x > 3 => false,
                    // Rule 4: Reproduction
                    (false, 3) => true,
                    (false, 6) => true,
                    // Otherwise: Stay dead
                    (_, _) => false,
                });
            }
        }

        self.cells = next;
    }

    /// Insert one universe into another
    pub fn insert(&mut self, other: &Universe, row: u32, column: u32) {
        for other_row in 0..other.height {
            for other_col in 0..other.width {
                let ins_row = (row + other_row) % self.height;
                let ins_col = (column + other_col) % self.width;

                let ins_idx = self.get_index(ins_row, ins_col);
                let other_idx = other.get_index(other_row, other_col);

                self.cells.set(ins_idx, other.cells[other_idx]);
            }
        }
    }

    /// Change width and height of the universe
    pub fn resize(&mut self, width: u32, height: u32) {
        let size = (width * height) as usize;
        let cells = FixedBitSet::with_capacity(size);

        let mut resized = Universe {
            width,
            height,
            cells,
        };

        resized.insert(self, 0, 0);
        *self = resized;
    }

    /// Toggle a single cell of the universe
    pub fn toggle(&mut self, row: u32, column: u32) {
        let idx = self.get_index(row, column);

        self.cells.toggle(idx);
    }

    /// return the universe as string
    pub fn render(&self) -> String {
        self.to_string()
    }
}

impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { 
        for row in 0..self.height {
            for col in 0..self.width {
                let idx = self.get_index(row, col);
                let symbol = if self.cells[idx] { '◼' } else { '◻' };
                write!(f, "{}", symbol)?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}
