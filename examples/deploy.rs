#[macro_use]
extern crate actix_web;

use std::{env, io};

use actix_files as fs;
use actix_web::http::{header, StatusCode};
use actix_web::{guard, middleware, web, App, HttpRequest, HttpResponse, HttpServer, Result};

/// favicon handler
#[get("/favicon")]
async fn favicon() -> Result<fs::NamedFile> {
    Ok(fs::NamedFile::open("www/favicon.ico")?)
}

/// simple index handler
#[get("/index.html")]
async fn index() -> Result<HttpResponse> {

    // response
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(include_str!("../www/index.html")))
}

async fn p404() -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::NOT_FOUND)
            .content_type("text/html; charset=utf-8")
            .body(include_str!("../www/404.html")))
    //Ok(fs::NamedFile::open("www/404.html")?.set_status_code(StatusCode::NOT_FOUND))
}

#[actix_rt::main]
async fn main() -> io::Result<()> {
    env::set_var("RUST_LOG", "actix_web=debug,actix_server=info");
    env_logger::init();

    HttpServer::new(|| {
        App::new()
                // enable logger - always register actix-web Logger middleware last
                .wrap(middleware::Logger::default())
                // register favicon
                .service(favicon)
                // register simple route, handle all methods
                .service(index)
                // static files
                .service(fs::Files::new("/www", "www").show_files_listing())
                .service(fs::Files::new("/pkg", "pkg").show_files_listing())
                // redirect
                .service(web::resource("/").route(web::get().to(|req: HttpRequest| {
                    println!("{:?}", req);
                    HttpResponse::Found()
                            .header(header::LOCATION, "index.html")
                            .finish()
                })))
                // default
                .default_service(
                    // 404 for GET request
                    web::resource("")
                            .route(web::get().to(p404))
                            // all requests that are not `GET`
                            .route(
                                web::route()
                                        .guard(guard::Not(guard::Get()))
                                        .to(HttpResponse::MethodNotAllowed),
                            ),
                )   
    })
    .bind("127.0.0.1:1337")?
    .run()
    .await
}